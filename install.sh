#!/bin/bash

# try to load ~/.artrc
[ -f ~/.artrc ] && source ~/.artrc

[ -z $ART_ROOT ] && ART_ROOT=/opt/art
[ -z $ART_REPO_ROOT ] && ART_REPO_ROOT=$ART_ROOT/p
[ -z $ARTCLI_BIN ] && ARTCLI_BIN=/usr/local/bin/art

REPO=platform-public
REPO_DIR="$ART_REPO_ROOT/$REPO"

die() {
  echo "$@" 1>&2
  exit 1
}

check_artcli(){
  echo "Checking $ARTCLI_BIN ..."
  [ -f $ARTCLI_BIN ]
}

install_artcli(){
  echo "Installing $ARTCLI_BIN ..."
  curl -sL https://raw.githubusercontent.com/art-ws/art-cli/master/install.sh | sudo bash -
  [ -f $ARTCLI_BIN ]
}

clone_repo(){
  local src=https://gitlab.com/aristsoft/$REPO.git
  echo "Cloning $src to $REPO_DIR ..."
  [ ! -d "$REPO_DIR/.git" ] && git clone $src "$REPO_DIR"
  [ ! -d "$REPO_DIR/.git" ] && die "Can't clone repository from $src".
  return 0
}

install(){
  echo "Installing $REPO ..."
  check_artcli || install_artcli
     
  clone_repo && \
    echo "$REPO installed successfully to $REPO_DIR" || \
    echo "$REPO installation failed"
}

verify(){
  echo "Verifying installation..."
  art os/about 
}

install && verify
