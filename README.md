# platform-public

art-ws.com cli tools

## Setup

### Install

`bash <(curl -s https://gitlab.com/aristsoft/platform-public/-/raw/master/install.sh)`

### Uninstall

`bash <(curl -s https://gitlab.com/aristsoft/platform-public/-/raw/master/uninstall.sh)`

### Requirements

- [Ubuntu](https://ubuntu.com/download)
- [art-cli](https://github.com/art-ws/art-cli) (optional)
