#!/bin/bash

# try to load ~/.artrc
[ -f ~/.artrc ] && source ~/.artrc

[ -z $ART_ROOT ] && ART_ROOT=/opt/art
[ -z $ART_REPO_ROOT ] && ART_REPO_ROOT=$ART_ROOT/p

REPO=platform-public
REPO_DIR=$ART_REPO_ROOT/$REPO

rm_dir(){
  [ -d "$1" ] && sudo rm -fR "$1"
  echo "Directory $1 not exists"
}

uninstall(){
  echo "Uninstalling $REPO ..."
  rm_dir $REPO_DIR
  return 0  
}

uninstall